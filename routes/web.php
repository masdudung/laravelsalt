<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/** frontpage */
Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();
// Route::resource('/operational', 'OperationalController')->middleware('auth');
//CRUD
/**
 * 1. route:get /operational -> controller@index
 * 
 * 2. route:get /operational/create -> controller@create
 * 3. route:post /operational/store -> controller@store
 * 
 * 4. route:get /operational/{id} -> controller@show
 * 
 * 5. route:get /operational/{id}/edit -> controller@edit
 * 6. route:patch/put /operational/{id} -> controller@update
 * 
 * 7. route:delete /operational/{id} -> controller@delete
 */



/** backoffice */
Route::group(['prefix'=>'admin', 'middleware'=>'auth', 'namespace' => 'Backend'], function(){
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::get('/revenue', 'RevenueController@index')->name('revenue');
    
    Route::resource('/transaction', 'TransactionController');

    Route::any('/operational/ajax', 'OperationalController@ajax')->name('operational.ajax');
    Route::resource('/operational', 'OperationalController');
});
