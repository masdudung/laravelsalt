@extends('layouts.app')

@section('content')
<div class="container-fluid" id="transactions-add-page">
    
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="row">
                <div class="col d-flex align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Add New Transactions</h6>
                </div>
            </div>
        </div>
        <div class="card-body">
        
            <form action="{{ route('transaction.store') }}" method="POST">
                @method('POST')
                @csrf
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group row">
                                <label class="col-md-2 label-control text-left">Transaction Date*</label>
                                <div class="col-md-10">
                                    <input type="date" class="form-control" id="date" name="date" value="{{ date('Y-m-d') }}">
                                    @error('date')
                                        <p class="block-tag">
                                            <small class="badge badge-default badge-danger">{{ $message }}</small>
                                        </p>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group row">
                                <label class="col-md-2 label-control text-left">Item*</label>
                                <div class="col-md-4">
                                    <select id="products" class="form-control">
                                        @foreach ($products as $product)
                                            <option value="{{ $product->id }}" data-price="{{ $product->selling_price }}">{{ $product->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <input class="form-control" id="quantity" type="number" value="1">
                                </div>
                                <a href="#" id="addDetail"><i class="fa fa-solid fa-plus"></i> more items</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group row">
                                <label class="col-md-2 label-control text-left"></label>
                                <div class="col-md-10">
                                    <table class="table-transactions table table-sm table-hover">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Product Name</th>
                                                <th scope="col">Quantity</th>
                                                <th scope="col">Price</th>
                                                <th scope="col">Subtotal</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group row">
                                <label class="col-md-2 label-control text-left"></label>
                                <div class="col-md-10">
                                    <a type="btn" class="btn btn-md btn-secondary" href="{{ route('transaction.index') }}">Back</a>
                                    <button type="submit" class="btn btn-md btn-primary">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
@endsection
