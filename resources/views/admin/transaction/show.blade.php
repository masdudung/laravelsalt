@extends('layouts.app')

@section('content')
<div class="container-fluid" id="transactions-show-page">
    
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="row">
                <div class="col d-flex align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Transactions Details</h6>
                </div>
            </div>
        </div>
        <div class="card-body">            
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group row">
                                <label class="col-md-2 label-control text-left">Transaction Date</label>
                                <div class="col-md-10">
                                    <p>{{ $transaction->date }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group row">
                                <label class="col-md-2 label-control text-left"></label>
                                <div class="col-md-10">
                                    <table class="table-transactions table table-sm table-hover">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Product Name</th>
                                                <th scope="col">Quantity</th>
                                                <th scope="col">Price</th>
                                                <th scope="col">Subtotal</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($transaction->details as $key => $detail)
                                            <tr>
                                                <td><p>{{ $key }}</p></td>
                                                <td><p>{{ $detail->product->name }}</p></td>
                                                <td><p>{{ $detail->quantity }}</p></td>
                                                <td><p>{{ $detail->current_selling_price }}</p></td>
                                                <td><p>{{ $detail->subtotal_selling_price }}</p></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group row">
                                <label class="col-md-2 label-control text-left">Purchase Price</label>
                                <div class="col-md-8">
                                    <p>{{ $transaction->total_purchase_price }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group row">
                                <label class="col-md-2 label-control text-left">Selling Price</label>
                                <div class="col-md-8">
                                    <p>{{ $transaction->total_selling_price }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group row">
                                <label class="col-md-2 label-control text-left">Profit</label>
                                <div class="col-md-8">
                                    <p>{{ $transaction->profit }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group row">
                                <label class="col-md-2 label-control text-left"></label>
                                
                            </div>
                        </div>
                    </div>
                </div>

        </div>
    </div>
</div>
@endsection
