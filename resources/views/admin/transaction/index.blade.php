@extends('layouts.app', ['title' => 'Transactions'])

@section('content')
<div class="container-fluid" id="transactions-page">
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="row">
                <div class="col d-flex align-items-center justify-content-between">
                    <a type="button" class="btn btn-sm btn-primary" href="{{ route('transaction.create') }}">Add New</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-sm table-hover">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Date</th>
                            <th scope="col">Selling Price</th>
                            <th scope="col">Purchase Price</th>
                            <th scope="col">Profit</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($transactions as $key => $item)
                        <tr>
                            <th scope="row">{{ $key + 1 }}</th>
                            <td>{{ $item->date }}</td>
                            <td>{{ $item->total_selling_price }}</td>
                            <td>{{ $item->total_purchase_price }}</td>
                            <td>{{ $item->profit }}</td>
                            <td></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                {{ $transactions->onEachSide(1)->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
