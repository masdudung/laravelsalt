@extends('layouts.app')

@section('content')
<div class="container-fluid">
    
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="row">
                <div class="col d-flex align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Operational Cost</h6>
                </div>
            </div>
        </div>
        <div class="card-body">
        
            <div class="form-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-md-2 label-control text-left">Title</label>
                            <div class="col-md-10">
                                <p>{{ $operational->title }}</p>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-md-2 label-control text-left">Price</label>
                            <div class="col-md-10">
                                <p>{{ $operational->price }}</p>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-md-2 label-control text-left">notes</label>
                            <div class="col-md-10">
                                <p>{{ $operational->notes }}</p>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-md-2 label-control text-left">Purchase Date</label>
                            <div class="col-md-10">
                                <p>{{ $operational->purchase_date }}</p>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-md-2 label-control text-left"></label>
                            <div class="col-md-10">
                                <a type="btn" class="btn btn-md btn-secondary" href="{{ route('operational.index') }}">Back</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        

        </div>
    </div>
</div>
@endsection
