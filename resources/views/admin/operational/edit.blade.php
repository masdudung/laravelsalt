@extends('layouts.app')

@section('content')
<div class="container-fluid">
    
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="row">
                <div class="col d-flex align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Edit Operational Cost</h6>
                </div>
            </div>
        </div>
        <div class="card-body">
        
            <form action="{{ route('operational.update', $operational->id) }}" method="POST">
                @method('PUT')
                @csrf
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group row">
                                <label class="col-md-2 label-control text-left">Title*</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" id="title" name="title" value="{{ $operational->title }}">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group row">
                                <label class="col-md-2 label-control text-left">Price*</label>
                                <div class="col-md-10">
                                    <input type="number" class="form-control" id="price" name="price" value="{{ $operational->price }}">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group row">
                                <label class="col-md-2 label-control text-left">notes</label>
                                <div class="col-md-10">
                                    <textarea class="form-control" id="notes" name="notes">{{ $operational->notes }}</textarea>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group row">
                                <label class="col-md-2 label-control text-left">Purchase Date*</label>
                                <div class="col-md-10">
                                    <input type="date" class="form-control" id="purchase_date" name="purchase_date" value="{{ $operational->purchase_date ?? date('Y-m-d') }}">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group row">
                                <label class="col-md-2 label-control text-left"></label>
                                <div class="col-md-10">
                                    <a type="btn" class="btn btn-md btn-secondary" href="{{ route('operational.index') }}">Back</a>
                                    <button type="submit" class="btn btn-md btn-primary">Edit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
@endsection
