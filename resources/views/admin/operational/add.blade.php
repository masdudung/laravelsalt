@extends('layouts.app')

@section('content')
<div class="container-fluid">
    
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="row">
                <div class="col d-flex align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Add New Operational Cost</h6>
                </div>
            </div>
        </div>
        <div class="card-body">
        
            <form action="{{ route('operational.store') }}" method="POST">
                @method('POST')
                @csrf
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group row">
                                <label class="col-md-2 label-control text-left">Title*</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" id="title" name="title">
                                    @error('title')
                                        <p class="block-tag">
                                            <small class="badge badge-default badge-danger">{{ $message }}</small>
                                        </p>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group row">
                                <label class="col-md-2 label-control text-left">Price*</label>
                                <div class="col-md-10">
                                    <input type="number" class="form-control" id="price" name="price">
                                    @error('number')
                                        <p class="block-tag">
                                            <small class="badge badge-default badge-danger">{{ $message }}</small>
                                        </p>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group row">
                                <label class="col-md-2 label-control text-left">notes</label>
                                <div class="col-md-10">
                                    <textarea class="form-control" id="notes" name="notes"></textarea>
                                    @error('notes')
                                        <p class="block-tag">
                                            <small class="badge badge-default badge-danger">{{ $message }}</small>
                                        </p>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group row">
                                <label class="col-md-2 label-control text-left">Purchase Date*</label>
                                <div class="col-md-10">
                                    <input type="date" class="form-control" id="purchase_date" name="purchase_date" value="{{ date('Y-m-d') }}">
                                    @error('purchase_date')
                                        <p class="block-tag">
                                            <small class="badge badge-default badge-danger">{{ $message }}</small>
                                        </p>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group row">
                                <label class="col-md-2 label-control text-left"></label>
                                <div class="col-md-10">
                                    <a type="btn" class="btn btn-md btn-secondary" href="{{ route('operational.index') }}">Back</a>
                                    <button type="submit" class="btn btn-md btn-primary">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
@endsection
