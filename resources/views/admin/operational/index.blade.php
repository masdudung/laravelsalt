@extends('layouts.app', ['title' => 'Operational'])

@section('content')
<div class="container-fluid" id="operational-page" data-ajax-url="{{ route('operational.ajax') }}">
    <script>
        var $lala = @json($lala) 
    </script>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="row">
                <div class="col d-flex align-items-center justify-content-between">
                    <a type="button" class="btn btn-sm btn-primary" href="{{ route('operational.create') }}">Add New</a>
                    
                    <form class="form-inline my-2 my-lg-0">
                        <!-- <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search"> -->
                        <form method="GET" action="{{ route('operational.index') }}">
                            <input type="date" class="form-control" name="purchase_date" value="{{ $purchaseDate }}">
                            <input type="submit" class="btn btn-sm btn-primary" value="filter">
                        </form>
                    </form>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-sm table-hover">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Title</th>
                            <th scope="col">Price</th>
                            <th scope="col">Notes</th>
                            <th scope="col">Purchase Date</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($operationals as $key => $item)
                        <tr>
                            <th scope="row">{{ $key + 1 }}</th>
                            <td>{{ $item->title }}</td>
                            <td>{{ $item->price }}</td>
                            <td>{{ $item->notes }}</td>
                            <td>{{ $item->purchase_date }}</td>
                            <td>
                                <div class="row">
                                    <div class="col-12">
                                        <a type="btn" class="btn btn-sm btn-secondary" href="{{ route('operational.show', $item->id) }}" >show</a>
                                        <a type="btn" class="btn btn-sm btn-secondary showData" id="{{ $item->id }}" >show ajax</a>
                                        <a type="btn" class="btn btn-sm btn-primary" href="{{ route('operational.edit', $item->id) }}">Edit</a>
                                    
                                        <form action="{{ route('operational.destroy', $item->id) }}" method="POST">
                                            @method('DELETE')
                                            @csrf
                                            <input type="submit" class="btn btn-sm btn-danger" value="delete">
                                        </form>
                                    </div>
                                </div>
                                
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div id="ajax-container">
    </div>
</div>
@endsection
