$('document').ready(function() {
    var $page = $('#operational-page'),
        $ajaxUrl = $('#operational-page').data('ajax-url')
    
    var operational = {
        init()
        {
            this.showDetail()
        },

        showDetail()
        {
            $('.showData').click(function(e) {
                e.preventDefault()
                
                var _id = $(this).attr('id')
                $.ajax({
                    method: "POST",
                    url: "https://laravelsalt.ss/admin/operational/ajax",
                    data: {id: _id}
                })

                .done(function( response ) {
                    // $('#ajax-container').empty()
                    // $('#ajax-container').append(`
                    // <p>${response.title}</p>
                    // <p>${response.price}</p>
                    // <p>${response.notes}</p>
                    // <p>${response.purchase_date}</p>
                    // `)
                    console.log(response)
                });
            })
        }
    }

    if($page.length){
        operational.init();
    }
})