$('document').ready(()=>{
    var $page = $('#transactions-add-page')
        $ajaxUrl = $('#operational-page').data('ajax-url')

    var $transactions = {
        init(){
            this.handleAddButton()    
        },

        handleAddButton(){
            $('#addDetail').click((e)=>{
                e.preventDefault()
                
                let detail = {
                    product_id: $('#products :selected').val(),
                    name: $('#products :selected').text(),
                    price: $('#products :selected').data('price'),
                    quantity: $('#quantity').val()
                }
                let subtotal = parseInt(detail.price) * parseInt(detail.quantity)

                let template = `<tr>
                <td>
                    <input type="hidden" name="productID[]" value="${detail.product_id}">
                    <input type="hidden" name="productQty[]" value="${detail.quantity}">
                </td>
                <td>${detail.name}</td>
                <td>${detail.quantity}</td>
                <td>${detail.price}</td>
                <td>${subtotal}</td>
                </tr>`;

                $('.table-transactions tbody').append(template)
            })
        }
    }

    if($page.length)
    {
        $transactions.init()
    }
})