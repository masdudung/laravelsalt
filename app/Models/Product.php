<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 
        'selling_price', 
        'purchase_price'
    ];

    protected $dateFormat = 'U';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $hidden = ['deleted_at'];

    public function transactionDetails()
    {
        return $this->hasMany(TransactionDetail::class);
    }
}
