<?php

namespace App\Http\Controllers\Backend;

use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Models\Product;
use App\Models\Revenue;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreTransactionsRequest;
use App\Http\Requests\UpdateTransactionsRequest;
use Illuminate\Http\Request;
use stdClass;

class TransactionController extends Controller
{
    private $transactions;
    private $products;

    public function __construct(
        Transaction $_transactions,
        Product $_products
    )
    {
        $this->transactions = $_transactions;
        $this->products = $_products;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = $this->transactions->paginate(10);
        return view('admin.transaction.index', compact('transactions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = $this->products->all();
        return view('admin.transaction.add', compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTransactionsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $transactionID = null;
        $date = $request->date ?? date('Y-m-d');
        if(is_array($request->productID) && count($request->productID) > 0)
        {
            /**init transaction model */
            $transaction = new Transaction();
            $transaction->date = $date;
            $transaction->total_selling_price = 0;
            $transaction->total_purchase_price = 0;
            $transaction->profit = 0;
            
            /**create temp data */
            $products = [];
            $transactionDetails = [];
            
            /** handel same ID on transaction & sum qty */
            foreach ($request->productID as $key => $productID) {
                if(isset($products[$productID]))
                {
                    $products[$productID] += $request->productQty[$key] ?? 0;
                }else{
                    $products[$productID] = $request->productQty[$key] ?? 0;
                }
            }

            /** calculate amount fot transaction detail */
            foreach ($products as $productID => $qty) {
                $product = Product::find($productID);
                if($product)
                {
                    /**init transaction details model */
                    $detailsItem = new TransactionDetail();
                    $detailsItem->current_selling_price = $product->selling_price;
                    $detailsItem->current_purchase_price = $product->purchase_price;
                    $detailsItem->quantity = $qty;
                    $detailsItem->product_id = $productID;
                    $detailsItem->subtotal_selling_price = $product->selling_price * $qty;
                    $detailsItem->subtotal_purchase_price = $product->purchase_price * $qty;

                    /**update transaction model value */
                    $transaction->total_selling_price += $detailsItem->subtotal_selling_price;
                    $transaction->total_purchase_price += $detailsItem->subtotal_purchase_price;
                    $transaction->profit += $transaction->total_selling_price - $transaction->total_purchase_price;

                    $transactionDetails[] = $detailsItem;
                }
            }

            /**if there is transaction details, save the model */
            if(count($transactionDetails)>0)
            {
                $transaction->save();
                foreach ($transactionDetails as $key => $item) {
                    $transaction->details()->save($item);
                }
                $transactionID = $transaction->id;

                /**update revenue table */
                $this->updateRevenue(
                    $transaction->date,
                    $transaction->total_selling_price,
                    $transaction->total_purchase_price,
                    $transaction->profit,
                );
            }
        }

        if($transactionID)
        {
            return redirect(route('transaction.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        $this->replicate($transaction);
        return view('admin.transaction.show', compact('transaction'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTransactionsRequest  $request
     * @param  \App\Models\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTransactionsRequest $request, Transaction $transaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        //
    }

    /**generate transaction & detail transaction, by duplicating 1 existing transaction */
    private function replicate($transaction)
    {
        /** we replicate 100 times */
        for ($i=0; $i <1000; $i++) {
            $newTransaction = $transaction->replicate();
            
            /**set diferent date each record */
            $nowInunix = strtotime("+ ". $i ." day");
            $newTransaction->date = date("Y-m-d", $nowInunix);
            $newTransaction->save();

            if($transaction->details->count() > 0){
                foreach ($transaction->details as $key => $item) {
                    # code...
                    $newItem = $item->replicate();
                    $newTransaction->details()->save($newItem);
                }
            }

            /**update revenue table */
            $this->updateRevenue(
                $newTransaction->date,
                $newTransaction->total_selling_price,
                $newTransaction->total_purchase_price,
                $newTransaction->profit,
            );
        }
    }

    /** update revenue table when user create transaction */
    private function updateRevenue($date, $sell, $purchase, $profit)
    {
        $revenue = Revenue::where('date', $date)->first();
        if($revenue)
        {
            $revenue->total_selling_price = $revenue->total_selling_price + $sell;
            $revenue->total_purchase_price = $revenue->total_purchase_price + $purchase;
            $revenue->profit = $revenue->profit + $profit;
        }else{
            $revenue = new Revenue();
            $revenue->total_selling_price = $sell;
            $revenue->total_purchase_price = $purchase;
            $revenue->profit = $profit;
        }
        $revenue->date = $date;

        $revenue->save();
    }
}
