<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Operational;
use Illuminate\Http\Request;

class OperationalController extends Controller
{
    private $operational;

    public function __construct(Operational $_operational)
    {
        $this->operational = $_operational;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $purchaseDate = $request->purchase_date;
        if($purchaseDate){
            $operationals = $this->operational->where('purchase_date', $purchaseDate)->get();
        }else{
            $operationals = $this->operational->all();
        }

        $purchaseDate = $purchaseDate ?? date('Y-m-d');
        return view('admin/operational/index', compact('operationals', 'purchaseDate'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.operational.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->operational->create([
            'title' => $request->title,
            'price' => $request->price,
            'purchase_date' => $request->purchase_date,
            'notes' => $request->notes,
        ]);

        return redirect(route('operational.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Operational  $operational
     * @return \Illuminate\Http\Response
     */
    public function show(Operational $operational)
    {
        return view('admin.operational.show', compact('operational'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Operational  $operational
     * @return \Illuminate\Http\Response
     */
    public function edit(Operational $operational)
    {
        return view('admin/operational/edit', compact('operational'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Operational  $operational
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $this->operational->where('id', $id)->first();

        $data->title = $request->title;
        $data->price = $request->price;
        $data->notes = $request->notes; 
        $data->purchase_date = $request->purchase_date;
        
        if($data->save()){
            return redirect(route('operational.index'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Operational  $operational
     * @return \Illuminate\Http\Response
     */
    public function destroy(Operational $operational)
    {
        if($operational->delete()){
            return redirect(route('operational.index'));
        }
    }

    /** handle ajax */
    public function ajax(Request $request)
    {
        $data = $this->operational->where('id', $request->id)->first();
        return $data;    
    }
}
