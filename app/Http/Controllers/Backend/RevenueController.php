<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Revenue;
use Illuminate\Http\Request;

class RevenueController extends Controller
{
    private $revenue;

    public function __construct(Revenue $_revenue)
    {
        $this->revenue = $_revenue;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /**pagination config */
        // $currentPage = $request->page ?? 1;
        // $perPage = 10; #limit
        // $offset = ($currentPage - 1) * $perPage;
        
        // $data = Revenue::limit($perPage)->offset($offset)->get();
        // return view('admin.revenue.index', compact('data', 'currentPage'));


        $data = Revenue::paginate(10);
        return view('admin.revenue.index', compact('data'));
    }
}
